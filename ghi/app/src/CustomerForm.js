import React, { useState} from 'react';

function CustomerForm() {
  const [formData, setFormData] = useState({
    first_name: '',
    last_name: '',
    address: '',
    phone_number: '',
  });

  const handleSubmit = async (event) => {
    event.preventDefault();

    const url = 'http://localhost:8090/api/customers/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    try {
      const response = await fetch(url, fetchConfig);
      if (response.ok) {
        setFormData({
          first_name: '',
          last_name: '',
          address: '',
          phone_number: '',
        });
        alert("Customer created successfully!");
      } else {
        alert("Failed to add the customer!");
      }
    } catch (e) {
      console.error("An error occurred during the fetch operation:", e);
      alert("Error occurred!");
    }
  }

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;

    setFormData({
      ...formData,
      [inputName]: value,
    });
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a customer</h1>
          <form onSubmit={handleSubmit} id="add-salesperson-form">

          <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="First Name" required type="text" name="first_name" id="first_name" className="form-control" value={formData.first_name} />
              <label htmlFor="name">First Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Last Name" required type="text" name="last_name" id="last_name" className="form-control" value={formData.last_name} />
              <label htmlFor="name">Last Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Address" required type="text" name="address" id="address" className="form-control" value={formData.address} />
              <label htmlFor="name">Address</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Phone Number" required type="number" name="phone_number" id="phone_number" className="form-control" value={formData.phone_number} />
              <label htmlFor="name">Phone Number</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  )
}

export default CustomerForm;
