import React, { useEffect, useState } from 'react';

function AppointmentForm() {
  const [formData, setFormData] = useState({
    date_time: '',
    reason: '',
    status: '',
    vin: '',
    technician: '',
  });

  const [technicians, setTechnicians] = useState([]);
  
  const handleSubmit = async (event) => {
    event.preventDefault();

    const url = 'http://localhost:8080/api/appointments/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    try {
      const response = await fetch(url, fetchConfig);
      if (response.ok) {
        setFormData({
       date_time: '',
       reason: '',
       status: '',
       vin: '',
       technician: '',
        });
        alert("Appointment created successfully!");
  
         
      } else {
        alert("Failed to add the Appointment!");
      }
    } catch (e) {
      console.error("An error occurred during the fetch operation:", e);
      alert("Error occurred!");
    }
  };

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;

    setFormData({
      ...formData,
      [inputName]: value,
    });
  };

  useEffect(() => {

    async function fetchTechnicians() {
      try {
        const url = 'http://localhost:8080/api/technicians/';
        const response = await fetch(url);
        if (!response.ok) {
          alert("Bad response while fetching technicians!");
        } else {
          const data = await response.json();
          setTechnicians(data.technicians);
        }
      } catch (e) {
        console.error("An error occurred during the fetch operation:", e);
        alert("Error occurred while fetching technicians!");
      }
    }
  fetchTechnicians();
 
  }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a sercive appointment</h1>
          <form onSubmit={handleSubmit} id="add-appointment-form">


            <div className="mb-3">
              <label htmlFor="technician"></label>
              <select onChange={handleFormChange} name="technician" id="technician" className="form-select" value={formData.technician}>
                <option value="">Choose a technician</option>
                {technicians.map(technician => {
                  return (
                    <option key={technician.id} value={technician.id}>{technician.first_name}</option>
                  );
                })}
              </select>
            </div>

            <div className="form-floating mb-3">
            <input onChange={handleFormChange} placeholder="Time" required type="time" name="time" id="time" className="form-control" value={formData.time} />
            <label htmlFor="year">Time</label>
          </div>
          
          <div className="form-floating mb-3">
            <input onChange={handleFormChange} placeholder="Date" required type="date" name="date_time" id="date_time" className="form-control" value={formData.date} />
            <label htmlFor="year">Date</label>
          </div>

          <div className="form-floating mb-3">
            <input onChange={handleFormChange} placeholder="Reason" required type="text" name="reason" id="reason" className="form-control" value={formData.reason} />
            <label htmlFor="reason">Reason</label>
          </div>

          <div className="form-floating mb-3">
            <input onChange={handleFormChange} placeholder="Vin" required type="text" name="vin" id="vin" className="form-control" value={formData.vin} />
            <label htmlFor="year">Vin</label>
          </div>

          <div className="form-floating mb-3">
            <input onChange={handleFormChange} placeholder="customer" required type="text" name="customer" id="customer" className="form-control" value={formData.customer} />
            <label htmlFor="year">Customer</label>
          </div>

            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  )
}

export default AppointmentForm;