import React, { useState, useEffect } from 'react';

function TechnicianPage({ first_name, last_name, employee_id, onDelete }) {
  const handleDelete = () => {
    const confirmDelete = window.confirm('Are you sure you want to delete this technician?');
    if (confirmDelete) {
      onDelete();
    }
  };

  return (
    <tr>
      <td>{first_name}</td>
      <td>{last_name}</td>
      <td>{employee_id}</td>
      <td>
        <button onClick={handleDelete}>Delete</button>
      </td>
    </tr>
  );
}



function TechnicianPageContainer() {
  const [techniciansData, setTechniciansData] = useState([]);

  useEffect(() => {
    const fetchTechniciansData = async () => { 
      const url = 'http://localhost:8080/api/technicians/';
      try {
        const response = await fetch(url);

        if (!response.ok) {
          throw new Error('Bad response!');
        } else {
          const data = await response.json();
          const technicians = data.technicians.map(async (technician) => {
            const detailUrl = `http://localhost:8080/api/technicians/${technician.id}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
              const details = await detailResponse.json();
              return {
                id: technician.id,
                first_name: details.first_name,
                last_name: details.last_name,
                employee_id: details.employee_id
               };
            } else {
              throw new Error('Error occurred, could not get details!');
            }
          });

          Promise.all(technicians)
            .then((technicianDetails) => setTechniciansData(technicianDetails))
            .catch((error) => console.error(error));
        }
      } catch (e) {
        console.error(e);
        alert('Error was raised!');
      }
    };

    fetchTechniciansData(); 
  }, []);

  const handleDeleteTechnician = async (technicianIndex) => {
    const technicianToDelete = techniciansData[technicianIndex]; 
    const deleteUrl = `http://localhost:8080/api/technicians/${technicianToDelete.id}`;
    try {
      const response = await fetch(deleteUrl, {
        method: 'DELETE',
      });

      if (response.ok) {
        setTechniciansData((prevTechnicians) => prevTechnicians.filter((_, index) => index !== technicianIndex));
      } else {
        throw new Error('Error occurred while deleting the technician!');
      }
    } catch (e) {
      console.error(e);
      alert('Error occurred while deleting the technician!');
    }
  };

  return (
    <div>
      <table id="technicianContainer" className="table table-striped">
        <thead>
          <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Employee ID</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {techniciansData.map((technician, index) => (
            <TechnicianPage
              key={index}
              first_name={technician.first_name}
              last_name={technician.last_name}
              employee_id={technician.employee_id}
              onDelete={() => handleDeleteTechnician(index)}
            />
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default TechnicianPageContainer;