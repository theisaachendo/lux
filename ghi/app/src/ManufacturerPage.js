import React, { useState, useEffect } from 'react';

function ManufacturerPage({ name}) {
  return (
    <tr>
      <td>{name}</td>
    </tr>
  );
}

function ManufacturerPageContainer() {
  const [manufacturersData, setManufacturersData] = useState([]);

  useEffect(() => {
    const fetchManufacturerData = async () => {
      const url = 'http://localhost:8100/api/manufacturers/';
      try {
        const response = await fetch(url);
        if (!response.ok) {
          throw new Error('Bad response!');
        } else {
          const data = await response.json();
          const manufacturers = data.manufacturers.map((manufacturer) => ({
            id: manufacturer.id,
            name: manufacturer.name,
          }));

          setManufacturersData(manufacturers);
        }
      } catch (e) {
        console.error(e);
        alert('Error was raised!');
      }
    };

    fetchManufacturerData();
  }, []);

  return (
    <div>
      <table id="manufacturerContainer" className="table table-striped">
        <thead>
          <h1>Manufacturers</h1>
          <tr>
            <th>Name</th>
          </tr>
        </thead>
        <tbody>
          {manufacturersData.map((manufacturer, index) => (
            <ManufacturerPage
              key={index}
              name={manufacturer.name}
            />
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default ManufacturerPageContainer;
