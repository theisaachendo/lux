import React, { useState, useEffect } from 'react';
function formatDateTime(dateTimeString) {
  const dateTime = new Date(dateTimeString);
  const date = dateTime.toISOString().split('T')[0];
  const time = dateTime.toISOString().split('T')[1].slice(0, 5);
  return { date, time };
}

function AppointmentPage({ onDelete, vin, date, time, technician, reason, customer }) {
  const handleDelete = () => {
    const confirmDelete = window.confirm('Are you sure you want to delete this appointment?');
    if (confirmDelete) {
      onDelete();
    }
  };

  return (
    <tr>
      <td>{vin}</td>
      <td>VIP</td>
      <td>{customer}</td>
      <td>
        <input type="date" value={date} disabled />
      </td>
      <td>
        <input type="time" value={time} disabled />
      </td>
      <td>{technician}</td>
      <td>{reason}</td>
      <td>
        <button onClick={handleDelete}>Delete</button>
      </td>
    </tr>
  );
}

function AppointmentPageContainer() {
  const [appointmentsData, setAppointmentsData] = useState([]);

  useEffect(() => {
    const fetchAppointmentData = async () => {
      const url = 'http://localhost:8080/api/appointments/';
      try {
        const response = await fetch(url);

        if (!response.ok) {
          throw new Error('Bad response!');
        } else {
          const data = await response.json();
          const appointments = data.appointments.map((appointment) => ({
            customer: appointment.customer,
            vin: appointment.vin,
            reason: appointment.reason,
            status: appointment.status,
            technician: appointment.technician.first_name + ' ' + appointment.technician.last_name,
            ...formatDateTime(appointment.date_time),   
          }));

          setAppointmentsData(appointments);
        }
      } catch (e) {
        console.error(e);
        alert('Error was raised!');
      }
    };

    fetchAppointmentData();
  }, []);

  const handleDeleteAppointment = async (appointmentIndex) => {
    const appointmentToDelete = appointmentsData[appointmentIndex];
    const deleteUrl = `http://localhost:8080/api/appointments/${appointmentToDelete.id}`;
    
    try {
      const response = await fetch(deleteUrl, {
        method: 'DELETE',
      });

      if (response.ok) {
        console.log('Appointment deleted successfully!');
        setAppointmentsData((prevAppointments) => prevAppointments.filter((_, index) => index !== appointmentIndex));
      } else {
        throw new Error('Error occurred while deleting the appointment!');
      }
    } catch (e) {
      console.error(e);
      alert('Error occurred while deleting the appointment!');
    }
  };

  return (
    <div>
      <table id="appointmentContainer" className="table table-striped">
        <thead>
          <tr>
            <th>Vin</th>
            <th>Is VIP?</th>
            <th>Customer</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {appointmentsData.map((appointment, index) => (
            <AppointmentPage
              key={index}
              vin={appointment.vin}
              customer={appointment.customer}
              date={appointment.date}
              time={appointment.time}
              technician={appointment.technician}
              reason={appointment.reason}
              status={appointment.status}
              onDelete={() => handleDeleteAppointment(index)}
            />
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default AppointmentPageContainer;