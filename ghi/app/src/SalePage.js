import React, { useState, useEffect } from 'react';

function SalePage({ onDelete, employee_id, salesperson, customer, vin, price }) {
  const handleDelete = () => {
    const confirmDelete = window.confirm('Are you sure you want to delete this sale?');
    if (confirmDelete) {
      onDelete();
    }
  };

  return (
    <tr>
      <td>{employee_id}</td>
      <td>{salesperson}</td>
      <td>{customer}</td>
      <td>{vin}</td>
      <td>{price}</td>
      <td>
        <button onClick={handleDelete}>Delete</button>
      </td>
    </tr>
  );
}

function SalePageContainer() {
  const [salesData, setSalesData] = useState([]);

  useEffect(() => {
    const fetchSaleData = async () => {
      const url = 'http://localhost:8090/api/sales/';
      try {
        const response = await fetch(url);

        if (!response.ok) {
          throw new Error('Bad response!');
        } else {
          const data = await response.json();
          const sales = data.sales.map((sale) => ({
            id: sale.id,
            employee_id: sale.salesperson.employee_id,
            salesperson: sale.salesperson.first_name,
            customer: sale.customer.first_name,
            vin: sale.automobile.vin,
            price: sale.price,

          }));

          setSalesData(sales);
        }
      } catch (e) {
        console.error(e);
        alert('Error was raised!');
      }
    };

    fetchSaleData();
  }, []);

  const handleDeleteSale = async (saleIndex) => {
    const saleToDelete = salesData[saleIndex];
    const deleteUrl = `http://localhost:8090/api/sales/${saleToDelete.id}`;
    try {
      const response = await fetch(deleteUrl, {
        method: 'DELETE',
      });

      if (response.ok) {
        setSalesData((prevSales) => prevSales.filter((_, index) => index !== saleIndex));
      } else {
        throw new Error('Error occurred while deleting the sale!');
      }
    } catch (e) {
      console.error(e);
      alert('Error occurred while deleting the sale!');
    }
  };

  return (
    <div>
      <table id="saleContainer" className="table table-striped">
        <thead>
          <h1>Sales</h1>
          <tr>
            <th>Salesperson EmployeeID</th>
            <th>Salesperson Name</th>
            <th>Customer</th>
            <th>VIN</th>
            <th>Price</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {salesData.map((sale, index) => (
            <SalePage
              key={index}
              employee_id={sale.employee_id}
              salesperson={sale.salesperson}
              customer={sale.customer}
              vin={sale.vin}
              price={sale.price}
              onDelete={() => handleDeleteSale(index)}
            />
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default SalePageContainer;
