import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">Home</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown1" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Manufacturers
              </a>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdown1">
                <li><NavLink className="dropdown-item" to="/manufacturers">List Manufacturers</NavLink></li>
                <li><NavLink className="dropdown-item" to="/manufacturers/new">Create Manufacturer</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown2" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Vehicle Models
              </a>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdown2">
                <li><NavLink className="dropdown-item" to="/vehiclemodels">List Models</NavLink></li>
                <li><NavLink className="dropdown-item" to="/vehiclemodels/new">Create Vehicle Model</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown3" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Automobiles
              </a>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdown3">
                <li><NavLink className="dropdown-item" to="/automobiles">List Automobiles</NavLink></li>
                <li><NavLink className="dropdown-item" to="/automobiles/new">Create Automobile</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown4" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Salespeople
              </a>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdown4">
                <li><NavLink className="dropdown-item" to="/salespeople">List Salespeople</NavLink></li>
                <li><NavLink className="dropdown-item" to="/salespeople/new">Create Salesperson</NavLink></li>
                <li><NavLink className="dropdown-item" to="/technicians/new">Create Technician</NavLink></li>
                <li><NavLink className="dropdown-item" to="/technicians">List Technicians</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown5" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Appointments
              </a>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdown5">
                <li><NavLink className="dropdown-item" to="/appointment/new">Schedule Service Appointment</NavLink></li>
                <li><NavLink className="dropdown-item" to="/appointments">List Service Appointments</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown6" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Customers
              </a>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdown6">
                <li><NavLink className="dropdown-item" to="/customers">List Customers</NavLink></li>
                <li><NavLink className="dropdown-item" to="/customers/new">Create Customer</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown7" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Sales
              </a>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdown7">
                <li><NavLink className="dropdown-item" to="/sales">List Sales</NavLink></li>
                <li><NavLink className="dropdown-item" to="/sales/new">Create Sale</NavLink></li>
                <li><NavLink className="dropdown-item" to="/saleshistory">Salesperson History</NavLink></li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
