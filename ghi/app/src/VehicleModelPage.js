import React, { useState, useEffect } from 'react';

function VehicleModelPage({ name, picture_url, manufacturer }) {



  return (
    <tr>
      <td>{name}</td>
      <td>{manufacturer}</td>
      <td>
        <img
          src={picture_url}
          alt="model picture"
          style={{ maxWidth: '100px', maxHeight: '100px' }}
        />
      </td>
    </tr>
  );
}

function VehicleModelPageContainer() {
  const [vehicleModelsData, setVehicleModelsData] = useState([]);

  useEffect(() => {
    const fetchVehicleModelData = async () => {
      const url = 'http://localhost:8100/api/models/';
      try {
        const response = await fetch(url);
        if (!response.ok) {
          throw new Error('Bad response!');
        } else {
          const data = await response.json();
          const vehicleModels = data.models.map((vehicleModel) => ({
            id: vehicleModel.id,
            name: vehicleModel.name,
            picture_url: vehicleModel.picture_url,
            manufacturer: vehicleModel.manufacturer.name, // Assuming the manufacturer name is directly accessible in the response
          }));

          setVehicleModelsData(vehicleModels);
        }
      } catch (e) {
        console.error(e);
        alert('Error was raised!');
      }
    };

    fetchVehicleModelData();
  }, []);


  return (
    <div>
      <table id="vehicleModelContainer" className="table table-striped">
        <thead>
          <h1>Models</h1>
          <tr>
            <th>Name</th>
            <th>Manufacturer</th>
            <th>Picture</th>
          </tr>
        </thead>
        <tbody>
          {vehicleModelsData.map((vehicleModel, index) => (
            <VehicleModelPage
              key={index}
              name={vehicleModel.name}
              picture_url={vehicleModel.picture_url}
              manufacturer={vehicleModel.manufacturer}
            />
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default VehicleModelPageContainer;
