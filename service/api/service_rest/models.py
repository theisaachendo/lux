from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    vin = models.CharField(max_length=200, unique=True)
    sold = models.BooleanField()


class Technician(models.Model):
    first_name = models.CharField(max_length=200,)
    last_name = models.CharField(max_length=200,)
    employee_id = models.IntegerField(unique=True)


class Appointment(models.Model):
    date_time = models.DateTimeField(max_length=100)
    reason = models.CharField(max_length=100)
    status = models.CharField(max_length=100)
    vin = models.CharField(max_length=200, unique=False)
    customer = models.CharField(max_length=200, unique=False)
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE,)

    def __str__(self):
        return self.reason

    def get_api_url(self):
        return reverse("api_show_appointment", kwargs={"pk": self.pk})
